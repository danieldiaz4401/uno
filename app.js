const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const uno = require('./uno.js').uno;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const PORT = 3000;

// Array de jugadores en la partida
const jugadores = [];
let partidaEnCurso = false;

// Manejador de conexión de socket
io.on('connection', socket => {
    console.log('Nuevo jugador conectado');

    // Manejador de evento para unirse a la partida
    socket.on('unirsePartida', nombreJugador => {
        if (partidaEnCurso) {
            console.log(`A ${nombreJugador} se le ha denegado el acceso a la partida`);
            return;
        }
        console.log(`${nombreJugador} se ha unido a la partida`);
        jugadores.push(nombreJugador);

        // Emitir evento 'jugadoresActualizados' a todos los jugadores
        io.emit('jugadoresActualizados', jugadores);
    });

    // Manejador de evento para empezar la partida y bloquear la sala
    socket.on('iniciarPartida', nombreJugador => {
        if (partidaEnCurso || jugadores.length < 2) {
            console.log(`A ${nombreJugador} se le ha denegado el acceso a la partida`);
            return;
        }
        partidaEnCurso = true;
        console.log(`${nombreJugador} ha iniciado la partida`);

        uno.iniciarPartida(jugadores);
        // Emitir evento 'jugadoresActualizados' a todos los jugadores
        io.emit('partidaIniciada', uno.estadoActual());
    });

    // Manejador de evento para jugar una carta
    socket.on('jugarCarta', (jugador, idCarta) => {

        if (uno.jugarCarta(jugador, idCarta)) {
            console.log(`${jugador} ha jugado la carta: ${idCarta} de su mano`);
            // Emitir evento 'estadoJuegoActualizado' a todos los jugadores
            const estadoJuego = uno.estadoActual();
            io.emit('estadoJuegoActualizado', estadoJuego);
        }
        console.log(`${jugador} no ha podido jugar la carta: ${idCarta} de su mano`);

    });

    socket.on('robar', jugador => {
        socket.emit('respuestaRobar', uno.robarCarta(jugador));
    });

    // Manejador de desconexión de socket
    socket.on('disconnect', () => {
        console.log('Jugador desconectado');

        // Remover al jugador de la partida
        const index = jugadores.indexOf(socket.id);
        if (index !== -1) {
            jugadores.splice(index, 1);
            // Emitir evento 'jugadoresActualizados' a todos los jugadores
            io.emit('jugadoresActualizados', jugadores);
        }
    });
});

// Iniciar el servidor
server.listen(PORT, () => {
    console.log(`Servidor iniciado en el puerto ${PORT}`);
});

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.get('/style.css', (req, res) => {
    res.sendFile(__dirname + '/style.css');
});