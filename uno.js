const SALA = {
    partidaIniciada: false,
    jugadores: [],
    descartes: [],
    pila: [],
    turno: 0,
    sentido: 1,
    haRobado: false,
    acumulado: 0,
    nuevoColor: 0,

    siguienteTurno() {
        this.haRobado = false;
        this.turno = (this.turno + this.sentido + this.jugadores.length) % this.jugadores.length;
        if (this.descartes[this.descartes.length - 1].numero == 12 && this.acumulado != 0) {
            if (this.jugadores[this.turno].cartas.filter(carta => carta.numero == 12).length == 0) {
                for (let i = 0; i < this.acumulado; i++) {
                    this.robarCarta();
                }
                this.acumulado = 0;
                this.siguienteTurno();
            }
        }
    },

    jugadorNuevo(nombre) {
        if (!this.partidaIniciada) {
            this.jugadores.push({ nombre: nombre, cartas: [] });
            return true;
        }
        return false;
    },

    tirar(jugador, i) {
        const cartaMano = this.jugadores.filter(e => e.nombre === jugador)[0].cartas[i];
        const cartaMazo = this.descartes[this.descartes.length - 1];
        const match = CARTAS.match(cartaMano, cartaMazo);
        if (match) {
            this.jugadores.filter(e => e.nombre === jugador)[0].cartas.splice(i, 1)[0];
            this.descartes.push(cartaMano);
            if (this.pila.length == 0) {
                console.log("Reciclando mazo");
                this.reciclarMazo();
            }
            if (cartaMano.numero == 10) {
                this.siguienteTurno();
            }
            if (cartaMano.numero == 11) {
                this.sentido *= -1;
            } else if (cartaMano.numero == 12) {
                this.acumulado += 2;
            }
        }
        return match;
    },

    reciclarMazo() {
        const ultimaCarta = this.descartes.splice(this.descartes.length - 1)[0];
        this.pila = CARTAS.barajar(this.descartes);
        this.descartes = [ultimaCarta];
    },

    robarCarta() {
        this.jugadores[this.turno].cartas.push(this.pila.pop());
    },

    repartirCartas() {
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < this.jugadores.length; j++) {
                this.jugadores[j].cartas.push(this.pila.pop());
            }
        }
    }


};


CARTAS = {
    // Las cartas son números que se referencian aquí
    // Cada color tiene:
    // Un cero (Posición 0)
    // Dos cartas 1-9 (Posición 1-18)
    // Dos salto de turno (Posición 19-20)
    // Dos cambios de sentido (Posición 21-22)
    // Dos robar 2 (Posición 23-24)
    // A parte, habran 8 wild cards:
    // 4 robar 4
    // 4 cambio de color
    // Por lo tanto, los colores quedan como:
    // Rojo 0-24
    // Amarillo 25-49
    // Verde 50-74
    // Azul 75-99
    // Wild Cards 100-107
    cartas: [],

    initCards() {
        for (let i = 0; i < 4; i++) {
            // Se añade el 0
            this.cartas.push({ numero: 0, color: i, mod: () => { } });
            // Se añaden los números del 1 al 9 2 veces
            for (let j = 0; j < 2; j++) {
                for (let k = 1; k <= 9; k++) {
                    this.cartas.push({ numero: k, color: i, mod: () => { } });
                }
            }
            // Se añaden los 2 saltos de turno
            for (let j = 0; j < 2; j++) {
                this.cartas.push({ numero: 10, color: i, mod: () => { } });
            }
            // Se añaden los 2 cambios de sentido
            for (let j = 0; j < 2; j++) {
                CARTAS.sentido *= -1;
                this.cartas.push({ numero: 11, color: i, mod: () => { } });
            }
            // Se añaden los 2 robar +2
            for (let j = 0; j < 2; j++) {
                this.cartas.push({ numero: 12, color: i, mod: () => { } });
            }
        }
        // Se añaden los 4 cambios de color
        for (let i = 0; i < 4; i++) {
            this.cartas.push({ numero: 13, color: -1, mod: () => { } });
        }
        // Se añaden los 4 robar +4
        for (let i = 0; i < 4; i++) {
            this.cartas.push({ numero: 14, color: -1, mod: () => { } });
        }
    },

    match(a, b) {
        console.log(`mano= color: ${a.color}, numero: ${a.numero}`);
        console.log(`mazo= color: ${b.color}, numero: ${b.numero}`);
        if (a.color == -1) {
            // Implementar dialogo para elegir color
            return true;
        } else if (b.color == -1) {
            // Mirar si se ha aplicado el castigo
            return a.numero == b.numero || a.color == this.colorActual;
        }
        return a.numero == b.numero || a.color == b.color;
    },

    print() {
        var res = "";
        this.cartas.forEach(carta => {
            res += `${this.printCard(carta)}`
        });
        document.getElementById("demo").innerHTML = res
    },

    barajar(cartas) {
        const aux = [];
        const length = cartas.length;
        for (let i = 0; i < length; i++) {
            const r = Math.floor(Math.random() * (cartas.length));
            aux[i] = cartas.splice(r, 1)[0];
        }
        console.log(aux)
        return aux;
    },

    getCarta(id) {
        return this.carta[id];
    }

}

function iniciarPartida(jugadores) {
    CARTAS.initCards();
    SALA.pila = CARTAS.barajar(CARTAS.cartas);
    jugadores.forEach(jugador => SALA.jugadorNuevo(jugador));
    SALA.repartirCartas();
    let primeraCarta = SALA.pila.pop();
    while (primeraCarta.numero > 9) {
        console.log("asdfa")
        SALA.pila.push(primeraCarta);
        primeraCarta = SALA.pila[Math.floor(Math.random() * SALA.pila.length)];

    }
    SALA.descartes.push(primeraCarta);
}

function jugarCarta(jugador, id) {
    if (SALA.jugadores[SALA.turno].nombre != jugador || !SALA.tirar(jugador, id)) {
        return false;
    }
    SALA.siguienteTurno();
    return true;
}

function pasarTurno() {
    SALA.siguienteTurno();
}

function finalizarPartida() {
    console.log(`Partida finalizada, el jugador ${SALA.jugadores[SALA.turno]} ha ganado.`)
}

function robarCarta(jugador) {
    if (SALA.haRobado) {
        pasarTurno();
    }
    const jugadorActual = SALA.jugadores[SALA.turno];
    if (jugadorActual.nombre === jugador) {
        if (jugadorActual.cartas.filter(carta => CARTAS.match(carta, SALA.descartes[SALA.descartes.length - 1])).length == 0) {
            SALA.robarCarta(jugador);
            SALA.haRobado = true;
        }
    }
    return estadoActual();
}

function estadoActual() {
    return { jugadores: SALA.jugadores, cartaMesa: SALA.descartes[SALA.descartes.length - 1], turno: SALA.turno }
}

exports.uno = {
    iniciarPartida,
    jugarCarta,
    pasarTurno,
    robarCarta,
    finalizarPartida,
    estadoActual
};